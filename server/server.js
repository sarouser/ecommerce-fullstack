const express = require("express")
const mongoose = require("mongoose")
const bodyParser = require("body-parser")
const morgan = require("morgan")
const cors = require("cors")
require("dotenv").config()
const {readdirSync} = require("fs");
// const authRoutes = require("./routes/auth");
const routes = require("./routes/routesConstructor");
// console.log("routes")
// console.log(routes)
// const {middleFunctions, noMiddleFunctions} = require("./controllers/middlewares");

//app
const app = express();

//db
const db_config = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: true
}
mongoose.connect(process.env.DATABASE, db_config)
    .then(() => console.log("DB connected"))
    .catch((err) => console.log("DB connection error -> ", err));

//middlewares
app.use(morgan("dev"))
app.use(bodyParser.json({limit: "2mb"}));
app.use(cors());

app.use("/api", routes)

// app.use("/api", (async (req, res, next) => {
//     let route = req.method + ' ' + req.url;
//     console.log("this is route")
//     // console.log(route)
//     let middleware = middleFunctions[route] || noMiddleFunctions;
//     let handler = routes[route] || noRouteFound;
//     await (middleware(req, res, next) || handler(req, res)) && handler(req, res);
// }));

// readdirSync("./routes").map((r) => {
//     app.use("/api", require(`./routes/${r}`));
//     console.log(`./routes/${r}`);
// })
//port
const port = process.env.PORT;

app.listen(port, () => console.log(`Listening to the port:${port}`));