const {routes} = require("./index");
const express = require("express")

function setupRoutes(routes) {


    const router = express.Router();
    // load all internal routes
    const routesKey = Object.keys(routes);
    // console.log("routkeys", routesKey)
    routesKey.forEach((routeKey) => {
        // console.log("key", routeKey)
        const splitted = routeKey.split(' ');
        const method = splitted[0];
        const path = splitted[1];
        const routerParams = [];
        let current = routes[routeKey];
        // console.log("curr", current)
        let func = () => console.log(1)
        // console.log("func:-", func)
        // if the route is marked as authenticated
        // routerParams.push(current.httpAdapter);
        if (current.middleware) {
            current.middleware.forEach(func => {
                // console.log("key func:", func)
                // console.log(func())
                routerParams.push(func)
                // console.log("routerParams")
                // console.log(routerParams)
                // console.log("func")
                // console.log(current.middleware[func])
                //         console.log("logggggggg", func)
            })
        }
        // add the controller at the end of the array
        // console.log("current.handler")
        // console.log(current.handler)
        routerParams.push(current.handler);

        // call the router with the params
        // equivalent to router.method(path , params) [params = middleware + handler]
        router[method.toLowerCase()](path, ...routerParams);
    });

    return router;
}
let finalRoutes = setupRoutes(routes);
(() => {
    console.log("start")
    setupRoutes(routes)
})()
module.exports = finalRoutes;