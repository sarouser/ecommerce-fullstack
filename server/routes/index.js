const {
        handle_CU_user, handle_user, handle_curr_user,
        createCategory, readCategory, updateCategory, removeCategory, listCategory,
        createSub, readSub, updateSub, removeSub, listSub, getSubs,
        createProduct, listAll,
        uploadimages, removeimages
    } = require("../controllers"),
    {middleFunctions} = require("../controllers/middlewares");
const noRouteFound = (req, res) => {
    let route = req.method + ' ' + req.url;
    res.end('You asked for ' + route);
};
const routes = {
    "POST /create-or-update-user": {handler: handle_CU_user},
    "GET /user": {handler: handle_user},
    "POST /current-user": {handler: handle_curr_user},
    "POST /current-admin": {handler: handle_curr_user},
    //Categories
    "POST /category": {handler: createCategory},//authcheck,admincheck
    "GET /category/:slug": {handler: readCategory}, //authcheck,admincheck
    "PUT /category/:slug": {handler: updateCategory}, //authcheck,admincheck
    "GET /categories": {handler: listCategory},// X
    "DELETE /category/:slug": {handler: removeCategory}, //authcheck,admincheck
    "GET /category/subs/:_id": {handler: getSubs}, //authcheck,admincheck
    //Sub Categories
    "POST /sub": {handler: createSub},//authcheck,admincheck
    "GET /sub/:slug": {handler: readSub}, //authcheck,admincheck
    "PUT /sub/:slug": {handler: updateSub}, //authcheck,admincheck
    "GET /subs": {handler: listSub},// X
    "DELETE /sub/:slug": {handler: removeSub}, //authcheck,admincheck
    "POST /product": {handler: createProduct},//authcheck,admincheck
    "GET /products/:count": {handler: listAll},//authcheck,admincheck
    //Cloudinary
    "POST /uploadimages": {handler: uploadimages},//authcheck,admincheck
    "POST /removeimages": {handler: removeimages},//authcheck,admincheck

};

for (let key in routes) {
    routes[key]["middleware"] = middleFunctions[key] || null;
    console.log("j")
    console.log(routes[key]["middleware"])
}
//check log
(() => console.log(routes))()

module.exports = {routes, noRouteFound};