const userModel = require("../model/user");
const Category = require("../model/category");
const Sub = require("../model/sub");
const slugify = require("slugify");
const Product = require("../model/product");
const cloudinary = require("cloudinary");

// CLOUDINARY_CLOUD_NAME = dlknpob8q
// CLOUDINARY_API_KEY = 918394987895715
// CLOUDINARY_API_SECRET = 5
// i5Vp5GRZUBcswnVQX9BhqpZE9Q

//Cloudinary

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET,
})

const uploadimages = async (req, res) => {
    let result = await cloudinary.uploader.upload(req.body.image, {
        public_id: `${Date.now()}`,
        resource_type: "auto" // jpeg,png
    });
    res.json({
        public_id: result.public_id,
        url: result.secure_url
    })
}
const removeimages = (req, res) => {
    let image_id = req.body.public_id;

    cloudinary.uploader.destroy(image_id, (err, ress) => {
        if (err) return res.json({
            success: false,
            err: err
        })
        return res.send("OK");
    })
}


//-------Prodcut

const listAll = async (req, res) => {
    console.log("listall")
    let products = await Product.find({})
        .limit(parseInt(req.params.count))
        .populate("category")
        .populate("subs")
        .sort([["createdAt" , "desc"]])
        .exec()
    res.json(products);
}

const createProduct = async (req, res) => {
    try {
        console.log(req.body);
        req.body.slug = slugify(req.body.title);
        const newProduct = await new Product(req.body).save();
        res.json(newProduct);
        console.log("success")
    } catch (err) {
        console.log(err);
        res.status(400).json({
            err: err.message,

        })
        res.status(400).send("Create product failed!");
    }
};

//------Sub

const createSub = async (req, res) => {
    try {

        const {name, parent} = req.body,
            sub = await new Sub({name, parent, slug: slugify(name).toLowerCase()}).save();

        console.log("Category")
        console.log(sub)
        res.json(sub);
    } catch (err) {
        console.log("Create sub failed", err)
        res.status(400).send("Create sub failed!")
    }
}
const updateSub = async (req, res) => {
    const {name, parent} = req.body;
    try {

        const updated =
            await Sub.findOneAndUpdate(
                {slug: req.params.slug},
                {name, parent, slug: slugify(name)},
                {new: true}
            );
        res.json(updated);
    } catch (err) {
        res.status(400).send("Sub update failed!")
    }
}
const readSub = async (req, res) => {
    let sub = await Sub.findOne({slug: req.params.slug}).exec();
    return res.json(sub);
}
const removeSub = async (req, res) => {
    try {
        const deleted = await Sub.findOneAndDelete({slug: req.params.slug})
        res.json(deleted);
    } catch (err) {
        res.status(400).send("Sub delete failed");
    }
}
const listSub = async (req, res) => {
    return res.json(await Sub.find({}).sort({createdAt: -1}).exec());
};

//------Category

const getSubs = async (req, res) => {
    Sub.find({parent: req.params._id}).exec((err, subs) => {
        if (err) {
            console.log("err of getSubs")
            res.json(err.message);
        }
        console.log("no err")
        res.json(subs);
    });
}

const createCategory = async (req, res) => {
    try {
        const {name} = req.body,
            category = await new Category({name, slug: slugify(name).toLowerCase()}).save();
        console.log("Category")
        console.log(category)
        res.json(category);
    } catch (err) {
        console.log("Category failed", err)
        res.status(400).send("Category category failed!")
    }
}

const updateCategory = async (req, res) => {
    const {name} = req.body;
    try {
        const updated =
            await Category.findOneAndUpdate(
                {slug: req.params.slug},
                {name, slug: slugify(name)},
                {new: true}
            );
        res.json(updated);
    } catch (err) {
        res.status(400).send("Category update failed!")
    }
}

const readCategory = async (req, res) => {
    let category = await Category.findOne({slug: req.params.slug}).exec();
    return res.json(category);
}
const removeCategory = async (req, res) => {
    try {
        const deleted = await Category.findOneAndDelete({slug: req.params.slug})
        res.json(deleted);
    } catch (err) {
        res.status(400).send("Category delete failed");
    }
}
const listCategory = async (req, res) => {
    return res.json(await Category.find({}).sort({createdAt: -1}).exec());
};


const handle_curr_user = async (req, res) => {
    userModel.findOne({email: req.user.email}).exec((err, user) => {
        if (err) throw new Error(err)
        res.json(user);
    })
}
const handle_CU_user = async (req, res) => {
    console.log("call 2")
    console.log(req.user)
    const {name, picture, email} = req.user;
    console.log("email", email)
    let user;
    try {
        user = await userModel.findOneAndUpdate({email}, {
            name: email.split('@')[0], picture
        }, {new: true});
    } catch (err) {
        console.log("err of catch findandupdate", err)
    }

    if (user) {
        res.json(user);
        console.log("user updated", user);
    } else {
        try {
            const newUser = await new userModel({
                email, name: email.split('@')[0], picture
            }).save();
            console.log("new User", newUser)
            res.json(newUser)
        } catch (err) {
            console.log("err of creating user:", err)
        }

    }
}
const handle_user = (req, res) => {
    res.json({
        data: "hey, node 2"
    });
}

module.exports = {
    handle_CU_user, handle_user, handle_curr_user,
    createCategory, readCategory, updateCategory, removeCategory, listCategory,
    createSub, readSub, updateSub, removeSub, listSub, getSubs,
    createProduct, listAll,
    uploadimages, removeimages
}