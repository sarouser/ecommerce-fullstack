const admin = require("../firebase");
const User = require("../model/user");

const adminCheck = async (req, res, next) => {
    console.log("adminCheck worked")
    const {email} = req.user;

    const adminUser = await User.findOne({email}).exec();
    console.log(adminUser.role)
    if (adminUser.role !== "admin") {
        res.status(403).json({
            err: "admin resource. Access denied!"
        });
    } else {
        next();
    }
}
const authCheck = async (req, res, next) => {
    try {
        console.log("call 1")
        // console.log("req.headers")
        console.log(req.headers.authtoken);
        const firebaseUser = await admin
            .auth().verifyIdToken(req.headers.authtoken);
        console.log("firebase user", firebaseUser);
        req.user = firebaseUser;
        console.log("calling next after this");
        return next();
    } catch (err) {
        console.log("err")
        console.log(err)
        res.status(401).json({
            err: "Invalid or expired token."
        });
    }
    // console.log("HEaders"); // token
    // console.log(req.headers); // token
};

const noMiddleFunctions = (req, res, next) => {
    console.log(`There is no middleware defined for route: ${req.method + req.url}`)
    return next();
}

const middleFunctions = {
    "POST /create-or-update-user": [authCheck],
    "GET /user": 0, //TODO try to ommit this one!
    "POST /current-user": [authCheck],
    "POST /current-admin": [authCheck, adminCheck],
    "DELETE /category/:slug": [authCheck, adminCheck],
    "POST /category": [authCheck, adminCheck],
    "GET /category/:slug": [authCheck, adminCheck],
    "PUT /category/:slug": [authCheck, adminCheck],
    "POST /prodocut": [authCheck, adminCheck],
    "POST /uploadimages": [authCheck, adminCheck],
    "POST /removeimages": [authCheck, adminCheck],
};

module.exports = {middleFunctions, noMiddleFunctions}
