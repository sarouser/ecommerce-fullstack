const mongoose = require("mongoose");
const {ObjectId} = mongoose;

const productSchema = new mongoose.Schema({
    title: {
        type: String,
        trim: true,
        required: true,
        maxLength: 32,
        text: true
    }, slug: {
        type: String,
        unique: true,
        lowerCase: true,
        text: true
    }, description: {
        type: String,
        required: true,
        maxLength: 2000,
        text: true
    }, price: {
        type: Number,
        required: true,
        trim: true,
        maxLength: 32
    }, category: {
        type: ObjectId,
        ref: "Category"
    }, subs: [{
        type: ObjectId,
        ref: "Sub"
    }],
    quantity: {
        type: Number,
        default: 0
    },
    images: {
        type: Array
    },
    shipping: {
        type: String,
        enum: ["Yes", "No"],
    }, color: {
        type: String,
        enum: ["Black", "Brown", "White", "Blue", "Silver"]
    }, Brand: {
        type: String,
        enum: ["Apple", "Samsung", "Microsoft", "Lenovo", "Asus"]
    }, rating: [{
        start: Number,
        postedBy: {type: ObjectId, ref: "User"}
    }]
}, {timestamp: true});

module.exports = mongoose.model("Product", productSchema);