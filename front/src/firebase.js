import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/analytics";
// import "firebase/firestore";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCQjicxtBfF4LcF1IeaZmAYTaXD-v63A10",
    authDomain: "ecommerce-59b42.firebaseapp.com",
    projectId: "ecommerce-59b42",
    storageBucket: "ecommerce-59b42.appspot.com",
    messagingSenderId: "287840464319",
    appId: "1:287840464319:web:9bb07b1ab2b20130d8050a",
    measurementId: "G-F14LP329D9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();
export const auth = firebase.auth()
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();