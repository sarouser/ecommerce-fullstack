import React, {useState, useEffect} from 'react';
import AdminNav from "../../components/nav/AdminNav";
import {toast} from "react-toastify";
import {useSelector} from "react-redux";
import {createProduct} from "../../functions/product";
import {Link} from "react-router-dom"
import {EditOutlined, DeleteOutlined, LoadingOutlined} from "@ant-design/icons"
import CategoryForm from "../../components/forms/CategoryForm";
import LocalSearch from "../../components/forms/LocalSearch";
import ProductCreateForm from "../../components/forms/ProductCreateForm";
import {getCategories, getCategorySubs} from "../../functions/category";
import FileUpload from "../../components/forms/FileUpload";


const initialState = {
    title: "",
    description: "",
    price: "",
    categories: [],
    category: "",
    subs: [],
    shipping: "",
    quantity: "50",
    images: [],
    colors: ["Black", "Brown", "White", "Blue", "Silver"],
    brands: ["Apple", "Samsung", "Microsoft", "Lenovo", "Asus"],
    color: "White",
    brand: "Apple"
};

const ProductCreate = () => {

    // function swap(items, l, r) {
    //     let temp = items[l];
    //     items[l] = items[r];
    //     items[r] = temp;
    // }
    //
    // function partition(items, l, r) {
    //     let p = Math.floor(items.length / 2),
    //         li = l,
    //         ri = r;
    //     while (li <= ri) {
    //         while (items[li] < p) {
    //             li += 1;
    //         }
    //         while (items[ri] > p) {
    //             ri -= 1;
    //         }
    //         if (li <= ri) {
    //             swap(items, li, ri);
    //             li += 1;
    //             ri -= 1;
    //         }
    //     }
    //     return li;
    // }
    // 0,1,3,-1
    // 0,1,-1,3

    // const {user} = useSelector(state => ({...state}));
    //
    const [values, setValues] = useState(initialState);
    const [subOptions, setSubOptions] = useState([]);
    const [showSub, setShowSub] = useState(false);
    const [loading, setLoading] = useState(false);


    //redux
    const {user} = useSelector((state) => ({...state}))

    const handleSubmit = (e) => {
        e.preventDefault()
        createProduct(values, user.token)
            .then(res => {
                console.log(res)
                window.alert(`${res.data.title} is created`);
                window.location.reload();
            })
            .catch(err => {
                console.log("err ", err)
                // if (err.response.status === 400) toast.error(err.response.data);
                toast.error(err.response.data.err);
            })
    }

    const handleChange = (e) => {
        setValues({...values, [e.target.name]: e.target.value});
        console.log(e.target.name, ":", e.target.value);
    }

    const handleCategoryChange = (e) => {
        e.preventDefault();
        console.log("Clicked category", e.target.value)
        setValues({...values, subs: [], category: e.target.value});
        getCategorySubs(e.target.value).then(res => {
            console.log("subs", res);
            setSubOptions(res.data);
        });
        setShowSub(true);
    }


    // const [categories, setCa tegories] = useState([]);
    // const [keyword, setKeyword] = useState("");
    useEffect(() => {
        loadCategories();
    }, [])


    const loadCategories = () =>
        getCategories().then((c) => setValues({...values, categories: c.data}))
    //
    //
    //
    //
    // const handleSubmit = (e) => {
    //     e.preventDefault();
    //     console.log(name);
    //     setLoading(true);
    //     createCategory({name}, user.token)
    //         .then(res => {
    //             console.log(res)
    //             setLoading(false);
    //             setName("");
    //             toast.success(`"${res.data.name}" is created`);
    //             loadCategories();
    //         })
    //         .catch(err => {
    //             setLoading(false);
    //             if (err.response.status === 400) toast.error(err.response.data);
    //         });
    // }
    //
    // const handleRemove = async (slug) => {
    //     if (window.confirm("Delete?")) {
    //         setLoading(true)
    //         removeCategory(slug, user.token)
    //             .then(res => {
    //                 setLoading(false);
    //                 toast.success(`${res.data.name} has been deleted!`)
    //                 loadCategories();
    //
    //             })
    //             .catch(err => {
    //                 setLoading(false);
    //                 if (err.response.status === 400) toast.error(err.response.data);
    //             })
    //     }
    //
    // }
    //
    // const searched = (keyword) => (c) => c.name.toLowerCase().includes(keyword);

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-2">
                    <AdminNav/>
                </div>
                <div className="col-md-10">
                    {loading ? <LoadingOutlined className="text-danger"/> : <h4>Product create</h4>}

                    <hr/>

{/*                    {*/}
{/*                        //change characters by given number*/}

{/*                        function adder(word, num) {*/}
{/*                            let alph = ("abcdefghijklmnopqrstuvwxyz").split("");*/}
{/*                            let output = "";*/}
{/*                            for (let i = 0; i < word.length; ++i) {*/}
{/*                                if (alph.indexOf(word[i].toLowerCase()) === -1) {*/}
{/*                                    output += word[i];*/}
{/*                                    continue;*/}
{/*                                }*/}
{/*                                let desired_indx = ((num % 26) + alph.indexOf(word[i].toLowerCase())) % 26*/}

{/*                                console.log(`${alph.indexOf(word[i])}:${desired_indx}`)*/}
{/*                                console.log(alph[desired_indx])*/}
{/*                                output += alph[desired_indx]*/}


{/*                            }*/}
{/*                            console.log(output)*/}
{/*                            return output;*/}
{/*                        }*/}
{/*                        adder("I love JavaScript!", 100)*/}
{/*//\end*/}


{/*                    }*/}
                    {JSON.stringify(values.images)}

                    <div className="p-3">
                        <FileUpload
                            values={values}
                            setValues={setValues}
                            loading={loading}
                            setLoading={setLoading}/>
                    </div>

                    <ProductCreateForm
                        setValues={setValues}
                        showSub={showSub}
                        handleSubmit={handleSubmit}
                        handleChange={handleChange}
                        values={values}
                        subOptions={subOptions}
                        handleCategoryChange={handleCategoryChange}
                    />
                </div>
            </div>
        </div>
    );
};
export default ProductCreate;