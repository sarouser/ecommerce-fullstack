import React, {useEffect, useState} from 'react';
import AdminNav from "../../components/nav/AdminNav";
import {getProductByCount} from "../../functions/product"
import AdminProductCard from "../../components/cards/AdminProductCard";

const AllProducts = () => {
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        loadAllProducts();
    }, [])

    const loadAllProducts = () => {
        setLoading(true);
        getProductByCount(6)
            .then((res) => {
                console.log(res)
                setProducts(res.data)
                setLoading(false)

            })
            .catch(err => {
                setLoading(false)
                console.log(err)
            });
    }

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-2">
                    <AdminNav/>
                </div>
                <div className="col">
                    {loading ? (<h4 className="text-danger">Loading...</h4>) :
                        (<h4>All Products</h4>)}
                    <div className="row">
                        {products.map(p => (
                            <div key={p._id} className="col-md-4 pb-3">
                                <AdminProductCard product={p}/>
                            </div>
                        ))}
                    </div>

                </div>
            </div>
        </div>
    )
}
export default AllProducts;