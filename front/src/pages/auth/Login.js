import React, {useState, useEffect} from 'react'
import {auth, googleAuthProvider} from "../../firebase"
import {toast, ToastContainer} from "react-toastify";
import {useDispatch, useSelector} from 'react-redux'
import 'react-toastify/dist/ReactToastify.css'
import {Button} from "antd";
import {useHistory, Link} from "react-router-dom";
import {
    MailOutlined,
    GoogleOutlined
} from '@ant-design/icons';
import {createOrUpdateUser} from '../../functions/auth';


const Login = () => {

    const [email, setEmail] = useState("vilhelmsaro@gmail.com");
    const [password, setPassword] = useState("qwerty");
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch();
    const history = useHistory();


    const roleBasedRedirect = (res) => {
        if (res.data.role === "admin") {
            history.push("/admin/dashboard");
        } else {
            history.push("user/history");
        }
    };

    const {user} = useSelector(state => ({...state}));
    useEffect(() => {
        if (user && user.token) history.push("/");
    }, [user,history]);

    const handleLogin = async (e) => {
        e.preventDefault();
        setLoading(true);
        console.table(email, password)
        try {
            const result = await auth.signInWithEmailAndPassword(email, password)

            const {user} = result;
            console.log("test", result)
            const idTokenResult = await user.getIdTokenResult()

            createOrUpdateUser(idTokenResult.token)
                .then(res => {
                    dispatch({
                        type: "LOGGED_IN_USER",
                        payload: {
                            name: res.data.name,
                            email: res.data.email,
                            token: idTokenResult.token,
                            role: res.data.role,
                            id: res.data._id
                        }
                    });
                    roleBasedRedirect(res);
                })
                .catch(reason => console.log("reason", reason))
            // history.push("/")
            console.log(result)
        } catch (error) {
            console.log(error)
            toast.error(error.message)
            setLoading(false)
        }
    }

    const googleLogin = async () => {
        auth.signInWithPopup(googleAuthProvider)
            .then(async (res) => {
                const {user} = res;
                const idTokenResult = await user.getIdTokenResult()
                createOrUpdateUser(idTokenResult.token)
                    .then(res => {
                        dispatch({
                            type: "LOGGED_IN_USER",
                            payload: {
                                name: res.data.name,
                                email: res.data.email,
                                token: idTokenResult.token,
                                role: res.data.role,
                                id: res.data._id
                            }
                        });
                        roleBasedRedirect(res);
                    })
                    .catch(reason => loginForm("reason", reason))
                // history.push("/")

            }).catch((err) => {
            console.log(err);
            toast.error(err.message)
        })
    }
    const loginForm = () => {
        return (
            <form onSubmit={handleLogin}>
                <div className="form-group">
                    <input
                        type="email"
                        className="form-control"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        placeholder="Mail"
                        autoFocus
                    />
                </div>
                <div className="form-group">
                    <input
                        type="password"
                        className="form-control"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        placeholder="Password"
                    />
                </div>
                <br/>

                <Button
                    onClick={handleLogin}
                    type="primary"
                    className="mb-3"
                    block
                    shape="round"
                    icon={<MailOutlined/>}
                    size="large"
                    disabled={(!(email && password.length >= 6))}
                >
                    Login with Email/Password
                </Button>

            </form>
        )
    }

    return (
        <div className="container p-5">
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    {loading ? <h4>Loading...</h4> :
                        <h4>Login</h4>}
                    {loginForm()}

                    {console.log("info!!!")}
                    {console.log(loading)}
                    <Button
                        onClick={googleLogin}
                        type="danger"
                        className="mb-3"
                        block
                        shape="round"
                        icon={<GoogleOutlined/>}
                        size="large"
                        disabled={(!(email && password.length >= 6))}
                    >
                        Login with Google
                    </Button>
                    <Link to="/forgot/password" className="float-right text-danger">
                        Forgot Password
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default Login;