import React, {useState, useEffect} from 'react'
import {auth, googleAuthProvider} from "../../firebase"
import {toast, ToastContainer} from "react-toastify";
import {useSelector} from 'react-redux'
import 'react-toastify/dist/ReactToastify.css'
import {useHistory, Link} from "react-router-dom";
import {
    MailOutlined,
    GoogleOutlined
} from '@ant-design/icons';

const ForgotPassword = ({history}) => {
    const [email, setEmail] = useState("");
    const [loading, setLoading] = useState(false);

    const {user} = useSelector(state => ({...state}));
    useEffect(() => {
        if (user && user.token) history.push("/");
    }, [user, history]);


    const handleSubmit = async (e) => {
        e.preventDefault();

        setLoading(true);
        console.log("env")
        console.log(process.env.REACT_APP_FORGOT_PASSWORD_REDIRECT_URL)
        const config = {
            url: process.env.REACT_APP_FORGOT_PASSWORD_REDIRECT_URL,
            handleCodeInApp: true
        };

        await auth.sendPasswordResetEmail(email, config)
            .then(() => {
                setEmail("");
                setLoading(false);
                toast.success("Check your email!");
            })
            .catch(err => {
                setLoading(false);
                console.log("err message in forgot pwd")
                console.log(err)
                toast.error(err.message)
            });
    };
    return (
        <div className="container ant-col-md-6 offset-md-3 p-5">
            {loading ? <h4>Loading</h4> : <h4>Forgot Password</h4>}
            <form onSubmit={handleSubmit}>
                <input type="email"
                       className="form-control"
                       value={email}
                       onChange={(e) => {
                           setEmail(e.target.value)
                       }}
                       placeholder="Type your email"
                       autoFocus
                />
                <br/>
                <button disabled={!email} className="btn btn-raised">
                    Submit
                </button>
            </form>

        </div>
    )
}

export default ForgotPassword;