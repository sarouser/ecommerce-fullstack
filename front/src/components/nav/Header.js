import {React, useState} from 'react'
import {Menu} from 'antd';
import {
    LogoutOutlined,
    AppstoreOutlined,
    SettingOutlined,
    UserOutlined,
    UserAddOutlined
} from '@ant-design/icons';
import {Link} from "react-router-dom"
import firebase from "firebase";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";

const {SubMenu, Item} = Menu;

const Header = () => {
    let history = useHistory();

    let dispatch = useDispatch();
    let {user} = useSelector((state) => ({...state}));
    const logout = () => {
        firebase.auth().signOut();
        dispatch({
            type: "LOGOUT",
            payload: null

        })
        history.push("/login")
    }
    const [current, setCurrent] = useState("home");
    const handleClick = (e) => {
        console.log(e.key);
        setCurrent(e.key);
    }
    return (
        <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
            <Item key="home" icon={<AppstoreOutlined/>}>
                <Link to="/">Home </Link>
            </Item>

            {!user &&
            <Item key="register" icon={<UserAddOutlined/>} className="float-right">
                <Link to="/register">Register</Link>
            </Item>}

            {!user &&
            <Item key="login" icon={<UserOutlined/>} className="float-right">
                <Link to="/login">Login</Link>
            </Item>
            }


            {user &&
            <SubMenu icon={<SettingOutlined/>} title={user.email && user.email.split('@')[0]} className="float-right">
                {
                    user && user.role === "subscriber" &&
                    <Item key="setting:1"><Link to="/user/history">Dashboard</Link></Item>
                }
                {
                    user && user.role === "admin" &&
                    <Item key="setting:1"><Link to="/admin/dashboard">Dashboard</Link></Item>
                }
                <Item icon={<LogoutOutlined/>} onClick={logout}>Logout 2</Item>
            </SubMenu>}


        </Menu>
    );
}

export default Header;