import React from "react";
import Resizer from "react-image-file-resizer";
import axios from "axios";
import {useSelector} from "react-redux"
import {Avatar, Badge} from "antd";

const FileUpload = ({values, setValues, loading, setLoading}) => {
    const {user} = useSelector(state => ({...state}));


    const fileUploadResize = (e) => {
        //resize
        let files = e.target.files;
        let allFiles = values.images;
        if (files) {
            for (let i = 0; i < files.length; ++i) {
                Resizer.imageFileResizer(files[i], 720, 720, "JPEG", 100, 0, (uri) => {
                    console.log("uri", uri);
                    axios.post(`${process.env.REACT_APP_API}/uploadimages`,
                        {image: uri}, {
                            headers: {authtoken: user ? user.token : ""}
                        })
                        .then(res => {
                            console.log("res of image upload")
                            console.log(res)
                            setLoading(false);
                            allFiles.push(res.data);
                            setValues({...values, images: allFiles});
                        })
                        .catch(err => {
                            console.log("err of img upload", err);
                            setLoading(false);

                        })
                }, "base64")
            }
        }
        //send to server
        //set url in parent class
    }

    const handleImageRemove = (public_id) => {
        setLoading(true);
        console.log("remove img", public_id);
        axios.post(`${process.env.REACT_APP_API}/removeimages`, {public_id}, {
            headers: {
                authtoken: user ? user.token : "",
            }
        })
            .then(res => {
                setLoading(false)
                const {images} = values;
                let filteredImages = images.filter(item => {
                    return item.public_id !== public_id
                });
                setValues({...values, images: filteredImages});
            })
            .catch(err => {
                console.log("remove img error", err)
                setLoading(false)
            })
    }

    return (
        <>
            <div className="row">
                {
                    values.images && values.images.map(image => (
                        <Badge count="X" key={image.public_id}
                               onClick={() => handleImageRemove(image.public_id)}
                               style={{cursor: "pointer"}}
                        >
                            <Avatar shape="square"
                                    src={image.url} size={100} className="ml-3"/>
                        </Badge>

                    ))
                }
            </div>
            <div className="row">
                <label className="btn btn-primary">Choose file
                    <input hidden type="file" multiple
                           accept="image/*" onChange={fileUploadResize}/>
                </label>
            </div>
        </>
    )
}

export default FileUpload;