import React, {useEffect} from 'react'
import {auth} from "./firebase";
import {useDispatch} from 'react-redux'
import ForgotPassword from "./pages/auth/ForgotPassword"
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'


import {Switch, Route} from 'react-router-dom'
import UserRoute from "./components/routes/UserRoute"
import AdminRoute from "./components/routes/AdminRoute"
import Login from './pages/auth/Login'
import Register from './pages/auth/Register'
import RegisterComplete from "./pages/auth/RegisterComplete";

import Home from './pages/Home'
import Header from './components/nav/Header';
import {currentUser} from "./functions/auth";
import History from "./pages/user/History";
import Password from "./pages/user/Password"
import CategoryCreate from "./pages/admin/category/CategoryCreate"
import CategoryUpdate from "./pages/admin/category/CategoryUpdate"
import SubCreate from "./pages/admin/Sub/SubCreate"
import SubUpdate from "./pages/admin/Sub/SubUpdate"
import ProductCreate from "./pages/product/ProductCreate"
import Whishlist from "./pages/user/Whishlist"
import AdminDashboard from "./pages/admin/AdminDashboard"
import AllProducts from "./pages/product/AllProducts"


const App = () => {
    console.log(6)
    const dispatch = useDispatch()
    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged(async (user) => {
            if (user) {
                const idTokenResult = await user.getIdTokenResult();
                currentUser(idTokenResult.token)
                    .then(res => {
                        dispatch({
                            type: "LOGGED_IN_USER",
                            payload: {
                                name: res.data.name,
                                email: res.data.email,
                                token: idTokenResult.token,
                                role: res.data.role,
                                id: res.data._id
                            }
                        })
                    })
                    .catch(err => console.log(err))
            }

            return () => unsubscribe();
        })
    }, [dispatch])

    return (
        <>
            <Header/>
            <ToastContainer/>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/register/complete" component={RegisterComplete}/>
                <Route exact path="/forgot/password" component={ForgotPassword}/>
                <UserRoute exact path="/user/history" component={History}/>
                <UserRoute exact path="/user/wishlist" component={Whishlist}/>
                <UserRoute exact path="/user/password" component={Password}/>
                <AdminRoute exact path="/admin/dashboard" component={AdminDashboard}/>
                <AdminRoute exact path="/admin/category" component={CategoryCreate}/>
                <AdminRoute exact path="/admin/category/:slug" component={CategoryUpdate}/>
                <AdminRoute exact path="/admin/sub" component={SubCreate}/>
                <AdminRoute exact path="/admin/sub/:slug" component={SubUpdate}/>
                <AdminRoute exact path="/admin/product" component={ProductCreate}/>
                <AdminRoute exact path="/admin/products" component={AllProducts}/>
            </Switch>
        </>
    );
};

export default App;