import axios from "axios";

export const getSubs = async () => {
    return await axios.get(`${process.env.REACT_APP_API}/subs`);
}

export const getSub = async (slug) => {
    return await axios.get(`${process.env.REACT_APP_API}/sub/${slug}`);
}

export const removeSub = async (slug, authtoken) => {
    return await axios.delete(`${process.env.REACT_APP_API}/sub/${slug}`, {
        headers: {
            authtoken,
        }
    });
}

export const updateSub = async (slug, sub, authtoken) => {
    console.log("sub")
    console.log(sub)
    return await axios.put(`${process.env.REACT_APP_API}/sub/${slug}`, sub, {
        headers: {
            authtoken,
        }
    });
}

export const createProduct = async (product, authtoken) => {
    console.log("Worked!");
    return await axios.post(`${process.env.REACT_APP_API}/product`, product, {
        headers: {
            authtoken,
        }
    });
}

export const getProductByCount = async (count) => {
    console.log("Worked!");
    return await axios.get(`${process.env.REACT_APP_API}/products/${count}`);
}
