import axios from "axios";
export const createOrUpdateUser = async (authToken) => {
    console.log("AUTHTOKEN:", authToken)
    console.log(`${process.env.REACT_APP_API}/create-or-update-user`)
    return await axios.post(`${process.env.REACT_APP_API}/create-or-update-user`, {}, {
        headers: {
            authToken
        }
    });
}


export const currentUser = async (authToken) => {
    console.log("AUTHTOKEN:", authToken)
    console.log(`${process.env.REACT_APP_API}/create-or-update-user`)
    return await axios.post(`${process.env.REACT_APP_API}/current-user`, {}, {
        headers: {
            authToken
        }
    });
}


export const currentAdmin = async (authToken) => {
    // console.log("AUTHTOKEN:", authToken)
    console.log(`${process.env.REACT_APP_API}/create-or-update-user`);
    return await axios.post(`${process.env.REACT_APP_API}/current-admin`, {}, {
        headers: {
            authToken
        }
    });
}